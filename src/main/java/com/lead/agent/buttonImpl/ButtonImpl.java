package com.lead.agent.buttonImpl;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.lead.agent.button.Button;
import com.lead.agent.button.Facebook;
import com.lead.agent.button.InnerButton;
import com.lead.agent.button.InnerData;

@Service
public class ButtonImpl implements Button 
{
	List<InnerButton> innerbuttonlist = new ArrayList<InnerButton>();
	Facebook fb = new Facebook();
	InnerData innerData = new InnerData();
	InnerButton button = new InnerButton();
	InnerButton button2 = new InnerButton();

	@Override
	public InnerData getButtonsYesNo() 
	{
		innerbuttonlist=new ArrayList<InnerButton>();;
		innerData=new InnerData();

		button.setText("Yes");
		button.setPostback("yes");
		innerbuttonlist.add(button);

		button2.setText("No");
		button2.setPostback("no");
		innerbuttonlist.add(button2);

		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);

		return innerData;
	}
	public InnerData getButtonsGender() 
	{
		innerbuttonlist=new ArrayList<InnerButton>();;
		innerData=new InnerData();

		button.setText("Male");
		button.setPostback("male");
		innerbuttonlist.add(button);

		button2.setText("Female");
		button2.setPostback("female");
		innerbuttonlist.add(button2);

		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);

		return innerData;
	}
	public InnerData letsProceed() 
	{
		innerbuttonlist=new ArrayList<InnerButton>();;
		innerData=new InnerData();

		button.setText("Lets Proceed");
		button.setPostback("Hi");
		innerbuttonlist.add(button);

		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);

		return innerData;
	}
}
