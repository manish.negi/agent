package com.lead.agent.button;

public interface Button 
{
	public InnerData getButtonsYesNo();
	public InnerData getButtonsGender();
	public InnerData letsProceed();
}
