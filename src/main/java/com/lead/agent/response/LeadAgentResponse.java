package com.lead.agent.response;

import org.springframework.stereotype.Service;

@Service
public class LeadAgentResponse 
{
	public String leadAgentResponse(String speech)
	{
		StringBuilder requestdata = new StringBuilder();
		/*requestdata.append("	{	");
		requestdata.append("	  \"payload\": {	");
		requestdata.append("	    \"google\": {	");
		requestdata.append("	      \"expectUserResponse\": true,	");
		requestdata.append("	      \"richResponse\": {	");
		requestdata.append("	        \"items\": [	");
		requestdata.append("	          {	");
		requestdata.append("	            \"simpleResponse\": {	");
		requestdata.append("	              \"textToSpeech\": \""+speech+"\"	");
		requestdata.append("	            }	");
		requestdata.append("	          }	");
		requestdata.append("	        ]	");
		requestdata.append("	      }	");
		requestdata.append("	    },	");
		requestdata.append("	    \"facebook\": {	");
		requestdata.append("	      \"text\": \"Hello, Facebook!\"	");
		requestdata.append("	    },	");
		requestdata.append("	    \"slack\": {	");
		requestdata.append("	      \"text\": \""+speech+"\"	");
		requestdata.append("	    }	");
		requestdata.append("	  }	");
		requestdata.append("	}	");*/
		
		
		requestdata.append("	{	");
		requestdata.append("	  \"fulfillmentText\": \""+speech+"\",	");
		requestdata.append("	  \"fulfillmentMessages\": [	");
		requestdata.append("	    {	");
		requestdata.append("	      \"text\": [	");
		requestdata.append("	        \""+speech+"\"	");
		requestdata.append("	      ]	");
		requestdata.append("	    }	");
		requestdata.append("	  ],	");
		requestdata.append("	  \"source\": \"example.com\",	");
		requestdata.append("	  \"payload\": {	");
		requestdata.append("	    \"google\": {	");
		requestdata.append("	      \"expectUserResponse\": true,	");
		requestdata.append("	      \"richResponse\": {	");
		requestdata.append("	        \"items\": [	");
		requestdata.append("	          {	");
		requestdata.append("	            \"simpleResponse\": {	");
		requestdata.append("	              \"textToSpeech\": \""+speech+"\"	");
		requestdata.append("	            }	");
		requestdata.append("	          }	");
		requestdata.append("	        ]	");
		requestdata.append("	      }	");
		requestdata.append("	    },	");
		requestdata.append("	    \"facebook\": {	");
		requestdata.append("	      \"text\": \"Hello, Facebook!\"	");
		requestdata.append("	    },	");
		requestdata.append("	    \"slack\": {	");
		requestdata.append("	      \"text\": \""+speech+"\"	");
		requestdata.append("	    }	");
		requestdata.append("	  },	");
		requestdata.append("	  \"outputContexts\": [	");
		requestdata.append("	    {	");
		requestdata.append("	      \"name\": \"projects/${PROJECT_ID}/agent/sessions/${SESSION_ID}/contexts/context name\",	");
		requestdata.append("	      \"lifespanCount\": 5,	");
		requestdata.append("	      \"parameters\": {	");
		requestdata.append("	        \"param\": \"param value\"	");
		requestdata.append("	      }	");
		requestdata.append("	    }	");
		requestdata.append("	  ],	");
		requestdata.append("	  \"followupEventInput\": {	");
		requestdata.append("	    \"name\": \"event name\",	");
		requestdata.append("	    \"languageCode\": \"en-US\",	");
		requestdata.append("	    \"parameters\": {	");
		requestdata.append("	      \"param\": \"param value\"	");
		requestdata.append("	    }	");
		requestdata.append("	  }	");
		requestdata.append("	}	");
		
		
		

		return requestdata.toString();
	}
}
