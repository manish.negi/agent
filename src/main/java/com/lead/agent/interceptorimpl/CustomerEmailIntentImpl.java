package com.lead.agent.interceptorimpl;

import java.text.MessageFormat;
import java.util.Map;
import java.util.ResourceBundle;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lead.agent.commons.BeanProperty;
import com.lead.agent.interceptor.CustomerEmailIntent;
import com.lead.agent.service.CreateLeadService;
import com.lead.agent.service.GetLeadService;
@Service
public class CustomerEmailIntentImpl implements CustomerEmailIntent 
{
	ResourceBundle resb = ResourceBundle.getBundle("application");
	@Autowired
	private CreateLeadService createLeadService;
	@Autowired
	private GetLeadService getLeadService;
	@Autowired
	private BeanProperty bean;

	String speech="";
	@Override
	public String customerEmailIntent(Map<String, Map<String, String>> map, String sessionId) {
		if(map.containsKey(sessionId))
		{
			System.out.println("START CRATE LEAD:");
			String response = createLeadService.createLeadAPI(map, sessionId);
			System.out.println("END CRATE LEAD:");
			JSONObject object = new JSONObject(response.toString());
			String soaStatusCode=object.getJSONObject("response").getJSONObject("responseData").get("soaStatusCode").toString();
			int soastatus=Integer.parseInt(soaStatusCode);
			if(soastatus==200){
			JSONObject res=(JSONObject)object.getJSONObject("response").getJSONObject("responseData").getJSONArray("createLeadWithRiderResponse").get(0);
			String leadId=res.get("leadID")+"";
			map.get(sessionId).put("leadId", leadId);
			String responseleadAPI=getLeadService.getLeadAPI(map, sessionId);
			/*if(!responseleadAPI.equalsIgnoreCase("Invalid input, missing either of LeadId,eQuoteNumber,mobilenumber and dob"))
			{*/
			JSONObject objectlead = new JSONObject(responseleadAPI.toString());
			JSONObject linkresponse=(JSONObject)objectlead.getJSONObject("response").getJSONObject("responseData").getJSONArray("leadData").get(0);
			String equoteNumber=linkresponse.getJSONObject("lead").get("equoteNumber")+"";
			map.get(sessionId).put("eQuote", equoteNumber);
			String link=bean.getReDirectURL();
			
			String message1 = resb.getString("email1");
			String message2 = resb.getString("email2");
			String part1 = MessageFormat.format(message1, equoteNumber);
			String part2 = MessageFormat.format(message2, link, equoteNumber);
			speech = part1+part2;
		}
		else{
				/*JSONObject res=(JSONObject)object.getJSONObject("response").getJSONObject("responseData").getJSONArray("createLeadWithRiderResponse").get(1);
				speech = res.get("leadStatus")+"";*/
				speech="Wrong channel Id has been passed.";
			}
		}
		else{
			speech="Internal Glitch please try after some time";
		}
		return speech;
	}
}
