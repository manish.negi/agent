package com.lead.agent.interceptorimpl;

import java.util.Map;
import java.util.ResourceBundle;

import org.springframework.stereotype.Service;

import com.lead.agent.interceptor.EmailIntent;

@Service
public class EmailIntentImpl implements EmailIntent 
{
	String speech="";
	ResourceBundle res = ResourceBundle.getBundle("application");
	@Override
	public String emailIntent(Map<String, Map<String, String>> map, String sessionId) {
		speech=res.getString("mobileNumber");
		return speech;
	}

}
