package com.lead.agent.interceptorimpl;

import java.util.Map;
import java.util.ResourceBundle;

import org.springframework.stereotype.Service;
import com.lead.agent.interceptor.CustomerMobileNumberIntent;
@Service
public class CustomerMobileNumberIntentImpl implements CustomerMobileNumberIntent 
{
	ResourceBundle res = ResourceBundle.getBundle("application");
	String speech="";
	@Override
	public String customerMobileNumberIntent(Map<String, Map<String, String>> map, String sessionId) {
		if(map.containsKey(sessionId))
		{
			speech=res.getString("mobileNumber2");
		}
		else{
			speech="Internal Glitch Please try after some time :- mobile";
		}
		return speech;
	}
}