package com.lead.agent.interceptorimpl;

import java.util.Map;
import java.util.ResourceBundle;

import org.springframework.stereotype.Service;

import com.lead.agent.interceptor.CustomerGenderIntent;
@Service
public class CustomerGenderIntentImpl implements CustomerGenderIntent{
	
	ResourceBundle res = ResourceBundle.getBundle("application");
	String speech="";
	String totalPremiumWGST="";
	@Override
	public String customerGenderIntent(Map<String, Map<String, String>> map, String sessionId) {
		if(map.containsKey(sessionId))
		{
			speech=res.getString("customerGender");
					
		}
		else{
			speech="Something went wrong! Please try again after some time: Gender";
		}
		return speech;
	}
}

