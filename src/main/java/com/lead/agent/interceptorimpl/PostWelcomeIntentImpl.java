package com.lead.agent.interceptorimpl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.lead.agent.interceptor.PostWelcomeIntent;

@Service
public class PostWelcomeIntentImpl implements PostWelcomeIntent
{
	String speech="";
	@Override
	public String PostWelcomeIntentCall(Map<String,Map<String,String>> map, String sessionId) 
	{
		speech=" Hi, Thanks for showing interest in Max Life Term insurance. I am your online assistant."; 
				/*" $$ Max Life Online Term Plan Plus (UIN - 104N092V03) offers benefits like <hr>" + 
				" <B>Coverage for 40 critical illnesses</B>\n"
				+ "<B> Long term coverage for 85 years</B> \n "
				+ "<B>Choice of regular income payout options</B>";*/
		if("second".equalsIgnoreCase(map.get(sessionId).get("name")+""))
		{
			speech="I need a few details to calculate customised premium for you.\n Please share your first name.";
		}
		return speech;
	}

}
