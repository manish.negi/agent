package com.lead.agent.interceptorimpl;

import java.util.ResourceBundle;

import org.springframework.stereotype.Service;
import com.lead.agent.interceptor.WelcomeIntent;

@Service
public class WelcomeIntentImpl implements WelcomeIntent 
{
	ResourceBundle res = ResourceBundle.getBundle("application");
	String speech="";
	@Override
	public String welcomeIntentCall(String name) {
		speech=res.getString("dob");
		return speech;
	}
}