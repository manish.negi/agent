package com.lead.agent.interceptor;

import java.util.Map;

public interface CustomerDOBIntent {
	public String customerDOBIntent(Map<String,Map<String,String>> map, String sessionId);
}
