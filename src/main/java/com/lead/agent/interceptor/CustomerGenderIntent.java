package com.lead.agent.interceptor;

import java.util.Map;

public interface CustomerGenderIntent {
	public String customerGenderIntent(Map<String,Map<String,String>> map, String sessionId);

}
