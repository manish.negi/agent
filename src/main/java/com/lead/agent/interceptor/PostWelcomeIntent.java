package com.lead.agent.interceptor;

import java.util.Map;

public interface PostWelcomeIntent 
{
	public String PostWelcomeIntentCall(Map<String,Map<String,String>> map, String sessionId);

}
