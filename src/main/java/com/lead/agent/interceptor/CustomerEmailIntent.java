package com.lead.agent.interceptor;

import java.util.Map;

public interface CustomerEmailIntent {
	public String customerEmailIntent(Map<String,Map<String,String>> map, String sessionId);
}
