package com.lead.agent.interceptor;

import java.util.Map;

public interface CustomerMobileNumberIntent {
	public String customerMobileNumberIntent(Map<String,Map<String,String>> map, String sessionId);

}
