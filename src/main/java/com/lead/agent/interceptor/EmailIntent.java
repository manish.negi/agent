package com.lead.agent.interceptor;

import java.util.Map;

public interface EmailIntent 
{
	public String emailIntent(Map<String,Map<String,String>> map, String sessionId);

}
