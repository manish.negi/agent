package com.lead.agent.interceptor;

import java.util.Map;

public interface CustomerSmokerIntent {
	public String customerSmokerIntent(Map<String,Map<String,String>> map, String sessionId);
}
