package com.lead.agent.commons;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class DataFromJson 
{
	String name="", gender="", custEmail="", mobileNum="", date="", smoke="" ;

	public String customerNameVariable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap, Map<String,String> internalMap)
	{
		/*Map<String,String> internalMap = new ConcurrentHashMap<String,String>();
		if(externalMap.containsKey(sessionId))
		{
			if(externalMap.get(sessionId).get("name").toString()==null && "".equalsIgnoreCase(externalMap.get(sessionId).get("name")+""))
			{
				try {
					name = object.getJSONObject("result").getJSONObject("parameters").get("customername")+"";
				} catch (Exception e) {
					name = "";
				}
			}
			else
			{
				name=externalMap.get(sessionId).get("name")+"";
			}
		}
		else
		{
			try {
				name = object.getJSONObject("result").getJSONObject("parameters").get("customername")+"";
				String str [] = name.split(" ");
				name=str[0];
				internalMap.put("name", name);
				externalMap.put(sessionId, internalMap);
			} catch (Exception e) {
				name = "";
			}
		}*/
		//Map<String,String> internalMap = new ConcurrentHashMap<String,String>();
		if(externalMap.containsKey(sessionId))
		{
			try {
				name = object.getJSONObject("result").getJSONObject("parameters").get("customername")+"";
				String str [] = name.split(" ");
				name=str[0];
				name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
				externalMap.get(sessionId).put("name", name);
			} catch (Exception e) {
				name = "";
			}
		}
		else
		{
			try {
				name = object.getJSONObject("result").getJSONObject("parameters").get("customername")+"";
				String str [] = name.split(" ");
				name=str[0];
				name = name.substring(0, 1).toUpperCase()+name.substring(1).toLowerCase();
				externalMap.get(sessionId).put("name", name);
			} catch (Exception e) {
				name = "";
			}
		}
		return name;
	}
	public String genderVariable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap, Map<String,String> internalMap)
	{
		//Map<String,String> internalMap = new ConcurrentHashMap<String,String>();
		if(externalMap.containsKey(sessionId))
		{
			try {
				gender = object.getJSONObject("result").getJSONObject("parameters").get("gender")+"";
				if("male".equalsIgnoreCase(gender)){
					gender="M";
					externalMap.get(sessionId).put("gender", gender);
				}
				else{
					gender="F";
					externalMap.get(sessionId).put("gender", gender);
				}
			} catch (Exception e) {
				gender = "";
			}
		}
		else
		{
			try {

				gender = object.getJSONObject("result").getJSONObject("parameters").get("gender")+"";
				if("male".equalsIgnoreCase(gender)){
					gender="M";
					externalMap.get(sessionId).put("gender", gender);
				}
				else{
					gender="F";
					externalMap.get(sessionId).put("gender", gender);
				}
			} catch (Exception e) {
				gender = "";
			}
		}
		return gender;
	}
	public String email_Variable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap, Map<String,String> internalMap)
	{
		//Map<String,String> internalMap = new ConcurrentHashMap<String,String>();
		if(externalMap.containsKey(sessionId))
		{
			try {
				custEmail = object.getJSONObject("result").getJSONObject("parameters").get("email")+"";
				custEmail=custEmail.replaceAll("\\s","");
				externalMap.get(sessionId).put("custEmail", custEmail);
			} catch (Exception e) {
				custEmail = "";
			}
		}
		else
		{
			try {
				custEmail = object.getJSONObject("result").getJSONObject("parameters").get("email")+"";
				custEmail=custEmail.replaceAll("\\s","");
				externalMap.get(sessionId).put("custEmail", custEmail);
			} catch (Exception e) {
				custEmail = "";
			}
		}
		return custEmail;
	}
	public String mobile_Variable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap, Map<String,String> internalMap)
	{
		//Map<String,String> internalMap = new ConcurrentHashMap<String,String>();
		if(externalMap.containsKey(sessionId))
		{
			try {
				mobileNum = object.getJSONObject("result").getJSONObject("parameters").get("mobilenum")+"";
				externalMap.get(sessionId).put("mobileNum", mobileNum);
			} catch (Exception e) {
				mobileNum = "";
			}
		}
		else
		{
			try {
				mobileNum = object.getJSONObject("result").getJSONObject("parameters").get("mobilenum")+"";
				externalMap.get(sessionId).put("mobileNum", mobileNum);
			} catch (Exception e) {
				mobileNum = "";
			}
		}
		return mobileNum;
	}
	public String date_Variable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap, Map<String,String> internalMap)
	{
		if(externalMap.containsKey(sessionId))
		{
			try {
				date = object.getJSONObject("result").getJSONObject("parameters").get("date")+"";
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat sdfFormat = new SimpleDateFormat("dd-MM-yyyy");
				Date date1 = sdf.parse(date);
				String strDate1 = sdfFormat.format(date1);
				date = strDate1;
				externalMap.get(sessionId).put("date", date);
			} catch (Exception e) {
				date = "";
			}
		}
		else
		{
			try {
				date = object.getJSONObject("result").getJSONObject("parameters").get("date")+"";
			} catch (Exception e) {
				date = "";
			}
		}
		return date;
	}
	public String smoke_Variable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap, Map<String,String> internalMap)
	{
		if(externalMap.containsKey(sessionId))
		{
			try {
				smoke = object.getJSONObject("result").getJSONObject("parameters").get("smoke")+"";
				if("smoker".equalsIgnoreCase(smoke) || "Yes".equalsIgnoreCase(smoke) || "smoke".equalsIgnoreCase(smoke)){
					smoke="Y";
					externalMap.get(sessionId).put("smoke", smoke);
				}
				else{
					smoke="N";
					externalMap.get(sessionId).put("smoke", smoke);
				}
			} catch (Exception e) {
				smoke = "";
			}
		}
		else
		{
			try {
				smoke = object.getJSONObject("result").getJSONObject("parameters").get("smoke")+"";
			} catch (Exception e) {
				smoke = "";
			}
		}
		return smoke;
	}
}
