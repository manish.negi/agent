package com.lead.agent.commons;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BeanProperty 
{
	@Value("${env.productpremium}")
	private String productpremium;
	
	@Value("${env.createLead}")
	private String createLead;
	
	@Value("${env.createGetLead}")
	private String getLead;
	
	@Value("${env.reDirectURL}")
	private String reDirectURL;
	
	@Value("${env.soaUserId}")
	private String soaUserId;
	
	@Value("${env.soaPassword}")
	private String soaPassword;
	
	@Value("${env.adoptionlogs}")
	private String adoptionlogs;
	
	@Value("${env.svgURL}")
	private String svgURL;
	
	@Value("${env.svgUserId}")
	private String svgUserId;
	
	@Value("${env.svgPassword}")
	private String svgPassword;

	public String getSoaUserId() {
		return soaUserId;
	}

	public void setSoaUserId(String soaUserId) {
		this.soaUserId = soaUserId;
	}

	public String getSoaPassword() {
		return soaPassword;
	}

	public void setSoaPassword(String soaPassword) {
		this.soaPassword = soaPassword;
	}

	public String getProductpremium() {
		return productpremium;
	}

	public void setProductpremium(String productpremium) {
		this.productpremium = productpremium;
	}

	public String getCreateLead() {
		return createLead;
	}

	public void setCreateLead(String createLead) {
		this.createLead = createLead;
	}

	public String getGetLead() {
		return getLead;
	}

	public void setGetLead(String getLead) {
		this.getLead = getLead;
	}

	public String getReDirectURL() {
		return reDirectURL;
	}

	public void setReDirectURL(String reDirectURL) {
		this.reDirectURL = reDirectURL;
	}

	public String getAdoptionlogs() {
		return adoptionlogs;
	}

	public void setAdoptionlogs(String adoptionlogs) {
		this.adoptionlogs = adoptionlogs;
	}

	public String getSvgURL() {
		return svgURL;
	}

	public void setSvgURL(String svgURL) {
		this.svgURL = svgURL;
	}

	public String getSvgUserId() {
		return svgUserId;
	}

	public void setSvgUserId(String svgUserId) {
		this.svgUserId = svgUserId;
	}

	public String getSvgPassword() {
		return svgPassword;
	}

	public void setSvgPassword(String svgPassword) {
		this.svgPassword = svgPassword;
	}
	
	
}
