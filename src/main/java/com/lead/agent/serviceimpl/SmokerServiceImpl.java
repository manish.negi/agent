package com.lead.agent.serviceimpl;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.lead.agent.commons.BeanProperty;
import com.lead.agent.service.SmokerService;

@Service
public class SmokerServiceImpl implements SmokerService
{
	@Autowired
	private BeanProperty bean;

	@Override
	public String smokerAPI(Map<String, Map<String, String>> map, String sessionId) {
		System.out.println("SMOKER API : - Inside Smoker API:: START");
		StringBuilder result = new StringBuilder();
		String output = new String();
		String soaCorrelationId = "CorelationId"+System.currentTimeMillis();
		HttpURLConnection conn = null;
		try {
			String userDate = map.get(sessionId).get("date");
			System.out.println("User Date :- "+userDate);
			int finalage=0;
			SimpleDateFormat sdfFormat = new SimpleDateFormat("dd-MM-yyyy");
			Date datef = new Date();
			String currentDate = sdfFormat.format(datef);
			System.out.println("SMOKER API : - Current Date :- "+currentDate);
			String str1[] = userDate.split("-");
			String str2[] = currentDate.split("-");
			int dob1= Integer.parseInt(str1[2]);
			int dob2= Integer.parseInt(str2[2]);
			
			int month1= Integer.parseInt(str1[1]);
			int month2= Integer.parseInt(str2[1]);
			//int finalmonth = month2-month1;
			//System.out.println("month"+finalmonth);
			if(month2>month1){
			 finalage = dob2-dob1;
			}
			else
			{
				 finalage=dob2-dob1-1;
			}
			
			String age=String.valueOf(finalage);
			System.out.println("SMOKER API : AGE IS  -"+age );
			int age1=Integer.parseInt(age);
			int policy_Term=85-age1;
			if(policy_Term>50)
			{
				policy_Term=50;
			}
			String policyTerm=String.valueOf(policy_Term);
			map.get(sessionId).put("policyTerm", policyTerm);
			System.out.println("--------------"+policy_Term);
			String gender=map.get(sessionId).get("gender");
			String smoker=map.get(sessionId).get("smoke");
			String extURL = bean.getProductpremium();
			URL url = new URL(extURL);
			conn = (HttpURLConnection) url.openConnection();
			HttpsURLConnection.setFollowRedirects(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			StringBuilder requestdata = new StringBuilder();
			requestdata.append("	{	");
			requestdata.append("	  \"request\": {	");
			requestdata.append("	    \"header\": {	");
			requestdata.append("	      \"soaCorrelationId\": \""+soaCorrelationId+"\",	");
			requestdata.append("	      \"soaAppId\": \"NEO\"	");
			requestdata.append("	    },	");
			requestdata.append("	    \"payload\": {	");
			requestdata.append("	      \"reqPlan\": [	");
			requestdata.append("	        {	");
			requestdata.append("	          \"planId\": \"TCOTP2\",	");
			requestdata.append("	          \"variantId\": \"SA\",	");
			requestdata.append("	          \"gender\": \""+gender+"\",	");
			requestdata.append("	          \"age\": \""+age+"\",	");
			requestdata.append("	          \"empDiscount\": \"N\",	");
			requestdata.append("	          \"planSumAssured\": \"10000000\",	");
			requestdata.append("	          \"policyTerm\": \""+policyTerm+"\",	");
			requestdata.append("	          \"policyPayTerm\": \""+policyTerm+"\",	");
			requestdata.append("	          \"smoke\": \""+smoker+"\",	");
			requestdata.append("	          \"mode\": \"Monthly\",	");
			requestdata.append("	          \"reqRider\": [	");
			requestdata.append("	          	");
			requestdata.append("	          ]	");
			requestdata.append("	        }	");
			requestdata.append("	      ]	");
			requestdata.append("	    }	");
			requestdata.append("	  }	");
			requestdata.append("	}	");
			System.out.println("SMOKER API REQUEST -- "+requestdata.toString());
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(requestdata.toString());
			writer.flush();
			try {
				writer.close();
			} catch (Exception e1) {
			}

			int apiResponseCode = conn.getResponseCode();
			if (apiResponseCode == 200) 
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
			}
			else
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception Occoured While Calling API's " + e);
		}
		return result.toString();

	}

}
