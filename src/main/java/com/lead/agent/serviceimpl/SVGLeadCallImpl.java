package com.lead.agent.serviceimpl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.lead.agent.commons.BeanProperty;
import com.lead.agent.service.SVGLeadCall;

@Service
public class SVGLeadCallImpl implements SVGLeadCall {

	@Autowired
	private BeanProperty beanProperty;
	
	
			
	@Override
	public void svgLeadCall(Map<String, Map<String, String>> ExternalMap, String sessionId) 
	{
		String name = ExternalMap.get(sessionId).get("name");
		String custEmail = ExternalMap.get(sessionId).get("custEmail");
		String mobileNum = ExternalMap.get(sessionId).get("mobileNum");
		String date = ExternalMap.get(sessionId).get("date");
		String gender=ExternalMap.get(sessionId).get("gender");
		String smoker=ExternalMap.get(sessionId).get("smoke");
		String finalDate = date.replaceAll("-", "/");
		String eQuote = ExternalMap.get(sessionId).get("eQuote")+"";
		
		String url = beanProperty.getSvgURL();
		String svgUserId = beanProperty.getSvgUserId();
		String svgPassword = beanProperty.getSvgPassword();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		StringBuilder sb=new StringBuilder();
		sb.append("	{	");
		sb.append("	  \"user\": \""+svgUserId+"\",	");
		sb.append("	  \"pass\": \""+svgPassword+"\",	");
		sb.append("	  \"camp\": 273,	");
		sb.append("	  \"ref_id\": \"\",	");
		sb.append("	  \"name\": \""+name+"\",	");
		sb.append("	  \"email\": \""+custEmail+"\",	");
		sb.append("	  \"city\": \"\",	");
		sb.append("	  \"date_of_birth\": \""+finalDate+"\",	");
		sb.append("	  \"mobile_number\": \""+mobileNum+"\",	");
		sb.append("	  \"gender\": \""+gender+"\",	");
		sb.append("	  \"income\": \"\",	");
		sb.append("	  \"equote\": \""+eQuote+"\",	");
		sb.append("	  \"smoker\": \""+smoker+"\",	");
		sb.append("	  \"utm_source\": \"\",	");
		sb.append("	  \"utm_medium\": \"\",	");
		sb.append("	  \"utm_campaign\": \"\",	");
		sb.append("	  \"utm_term\": \"\",	");
		sb.append("	  \"utm_keyword\": \"\",	");
		sb.append("	  \"utm_content\": \"\",	");
		sb.append("	  \"client_ip\": \"\",	");
		sb.append("	  \"user_agent\": \"\",	");
		sb.append("	  \"status\": \"\"	");
		sb.append("	}	");
		HttpEntity<String> entity=new HttpEntity<String>(sb.toString(),	headers);
		RestTemplate restTemplate=new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity,String.class);
		if(response.getStatusCodeValue() == 200)
		{
			String responses = response.getBody();
			System.out.println("--------"+sessionId+"---------"+responses);
		}
	}
}

