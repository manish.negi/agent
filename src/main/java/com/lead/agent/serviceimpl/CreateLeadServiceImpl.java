package com.lead.agent.serviceimpl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.ResourceBundle;

import javax.net.ssl.HttpsURLConnection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lead.agent.commons.BeanProperty;
import com.lead.agent.service.CreateLeadService;

@Service
public class CreateLeadServiceImpl implements CreateLeadService 
{
	@Autowired
	private BeanProperty bean;

	@Override
	public String createLeadAPI(Map<String, Map<String, String>> map, String sessionId) {
		StringBuilder result = new StringBuilder();
		String output = new String();
		String soaUserId=bean.getSoaUserId();
		String soaPassword=bean.getSoaPassword();
		String totalPremiumWGST=map.get(sessionId).get("totalPremiumWGST")+"";
		double totalPremiumWGSTInteger=Double.parseDouble(totalPremiumWGST);
		String TotalPremiumWOGST=map.get(sessionId).get("TotalPremiumWOGST")+"";
		String str[]=TotalPremiumWOGST.split("\\.");
		String TotalPremiumWOGSTModified=str[0];
		double TotalPremiumWOGSTInteger=Double.parseDouble(TotalPremiumWOGST);
		double finalGST = totalPremiumWGSTInteger-TotalPremiumWOGSTInteger;
		DecimalFormat df = new DecimalFormat("0.##"); 
		String serviceTax=String.valueOf(df.format(finalGST));
		DateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
		Date date=new Date();
		System.out.println("Date in format :: "+dateFormat.format(date));
		System.out.println("-----------"+serviceTax);
		HttpURLConnection conn = null;
		try {
			String name=map.get(sessionId).get("name");
			String mobileNum=map.get(sessionId).get("mobileNum");
			String email=map.get(sessionId).get("custEmail");
			String gender=map.get(sessionId).get("gender");
			String smoker=map.get(sessionId).get("smoke");
			String dob=map.get(sessionId).get("date");
			String leadSource=map.get(sessionId).get("channel")+"";
			String influencerCategory=map.get(sessionId).get("category")+"";
			String influencerCompanyCode=map.get(sessionId).get("company")+"";
			String influencerSourceCode=map.get(sessionId).get("source")+"";
			String extURL = bean.getCreateLead();
			URL url = new URL(extURL);
			conn = (HttpURLConnection) url.openConnection();
			HttpsURLConnection.setFollowRedirects(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			StringBuilder requestdata = new StringBuilder();
			requestdata.append("	{	");
			requestdata.append("	  \"request\": {	");
			requestdata.append("	    \"header\": {	");
			requestdata.append("	      \"soaUserId\": \""+soaUserId+"\",	");
			requestdata.append("	      \"soaCorrelationId\": \"25478965874\",	");
			requestdata.append("	      \"soaPassword\": \""+soaPassword+"\",	");
			requestdata.append("	      \"soaMsgVersion\": \"1.0\",	");
			requestdata.append("	      \"soaAppId\": \"NEO\"	");
			requestdata.append("	    },	");
			requestdata.append("	    \"requestData\": {	");
			requestdata.append("	      \"createLeadWithRider\": [	");
			requestdata.append("	        {	");
			requestdata.append("	          \"proposer\": {	");
			requestdata.append("	            \"ppt\": \"18\",	");
			requestdata.append("	            \"varid\": \"6\",	");
			requestdata.append("	            \"email\": \""+email+"\",	");
			requestdata.append("	            \"sumAssured\": 10000000,	");
			requestdata.append("	            \"mobilePhone\": \""+mobileNum+"\",	");
			requestdata.append("	            \"isEligibleForPaymentFirst\": \"Yes\",	");
			requestdata.append("	            \"firstName\": \""+name+"\",	");
			requestdata.append("	            \"dob\": \""+dob+"\",	");
			requestdata.append("	            \"gender\": \""+gender+"\",	");
			requestdata.append("	            \"smoker\": \""+smoker+"\",	");
			requestdata.append("	            \"modalPremiumRequired\": \""+TotalPremiumWOGSTModified+"\",	");
			requestdata.append("	            \"lastName\": \"_\"	");
			requestdata.append("	          },	");
			requestdata.append("	          \"lifeToBeInsured\": {	");
			requestdata.append("	            \"areYouTheLifeToBeInsured?\": \"1\"	");
			requestdata.append("	          },	");
			requestdata.append("	          \"lead\": {	");
			requestdata.append("	            \"duration\": \""+map.get(sessionId).get("policyTerm")+""+"\",	");
			requestdata.append("	            \"dropOff\": \"Browser close\",	");
			requestdata.append("	            \"influencerCategory\": \""+influencerCategory+"\",	");
			requestdata.append("	            \"influencerSourceCode\": \""+influencerSourceCode+"\",	");
			requestdata.append("	            \"ratingKey\": \"1\",	");
			requestdata.append("	            \"leadSource\": \""+leadSource+"\",	");
			requestdata.append("	            \"influencerCompanyCode\": \""+influencerCompanyCode+"\",	");
			requestdata.append("	            \"agentState\": \"HR\",	");
			requestdata.append("	            \"agentCode\": \"481310\",	");
			requestdata.append("	            \"isEmployee\": \"N\",	");
			requestdata.append("	            \"productType\": \"5\",	");
			requestdata.append("	            \"amphetamines\": \"6\",	");
			requestdata.append("	            \"leadName\": \"create lead\",	");
			requestdata.append("	            \"leadCreatedDate\": \""+dateFormat.format(date)+"\",	");
			requestdata.append("	            \"applicationModifyDate\": \""+dateFormat.format(date)+"\",	");
			requestdata.append("	            \"equoteGenerated\": \""+dateFormat.format(date)+"\",	");
			requestdata.append("	            \"applicationSubStatus\": \"112\",	");
			requestdata.append("	            \"applicationStatus\": \"109\",	");
			requestdata.append("	            \"statusCode\": \"123\",	");
			requestdata.append("	            \"productCode\": \"TNOTP2\",	");
			requestdata.append("	            \"leadAmount\": \"0\",	");
			requestdata.append("	            \"currentPlanPremium\": "+map.get(sessionId).get("TotalPremiumWOGST")+",	");
			requestdata.append("	            \"planServiceTax\": "+serviceTax+",	");
			requestdata.append("	            \"totalPremium\": "+map.get(sessionId).get("totalPremiumWGST")+",	");
			requestdata.append("	            \"totalServiceTax\": "+serviceTax+",	");
			requestdata.append("	            \"ugstCess\": 0,	");
			requestdata.append("	            \"ugstRate\": 0,	");
			requestdata.append("	            \"sgstCess\": 0,	");
			requestdata.append("	            \"sgstRate\": 0.09,	");
			requestdata.append("	            \"igstRate\": 0,	");
			requestdata.append("	            \"igstCess\": 0,	");
			requestdata.append("	            \"cgstCess\": 0,	");
			requestdata.append("	            \"cgstRate\": 0.09,	");
			requestdata.append("	            \"frequencyOfPayment\": \"M\",	");
			requestdata.append("	            \"isEcsMail\": \"NO\",	");
			requestdata.append("	            \"monthFlag\": \"N\",	");
			requestdata.append("	            \"leadAction\": \"add\",	");
			requestdata.append("	            \"deviceUserFlag\": \"FALSE\",	");
			requestdata.append("	            \"leadOwnerKey\": \"1\",	");
			requestdata.append("	            \"statusCodeDisplay\": \"New Lead\",	");
			requestdata.append("	            \"isEcs\": \"0\",	");
			requestdata.append("	            \"emailQuoteCheckbox\": \"1\"	");
			requestdata.append("	          },	");
			requestdata.append("	          \"riders\": {	");
			requestdata.append("	            \"rider\": [	");
			requestdata.append("	              	");
			requestdata.append("	            ]	");
			requestdata.append("	          }	");
			requestdata.append("	        }	");
			requestdata.append("	      ]	");
			requestdata.append("	    }	");
			requestdata.append("	  }	");
			requestdata.append("	}	");

			System.out.println("CREATE LEAD REQUEST :-"+requestdata.toString());

			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(requestdata.toString());
			writer.flush();
			try {
				writer.close();
			} catch (Exception e1) {
			}
			int apiResponseCode = conn.getResponseCode();
			System.out.println("Create Lead API Calling Response Code:- "+apiResponseCode);
			if (apiResponseCode == 200) 
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
			}
			else
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
			}
		}
		catch(Exception e)
		{
			
			System.out.println("Exception Occoured While Calling API's " + e);
		}
		return result.toString();
	}
}
