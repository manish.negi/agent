package com.lead.agent.service;

import java.util.Map;

public interface SVGLeadCall 
{
	public void svgLeadCall(Map<String, Map<String, String>> map, String sessionId);
}
