package com.lead.agent.service;

import java.util.Map;

public interface CreateLeadService 
{
	public String createLeadAPI(Map<String, Map<String, String>> map, String sessionId);

}
