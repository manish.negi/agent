package com.lead.agent.service;

import java.util.Map;

public interface GetLeadService {
	public String getLeadAPI(Map<String, Map<String, String>> map, String sessionId);

}
