package com.lead.agent.service;

import java.util.Map;

public interface SmokerService 
{
	public String smokerAPI(Map<String, Map<String, String>> map, String sessionId);

}
