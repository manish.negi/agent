package com.lead.agent.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lead.agent.button.Button;
import com.lead.agent.button.InnerData;
import com.lead.agent.commons.Adoptionlogs;
import com.lead.agent.commons.DataFromJson;
import com.lead.agent.interceptor.CustomerDOBIntent;
import com.lead.agent.interceptor.CustomerEmailIntent;
import com.lead.agent.interceptor.CustomerGenderIntent;
import com.lead.agent.interceptor.CustomerMobileNumberIntent;
import com.lead.agent.interceptor.CustomerSmokerIntent;
import com.lead.agent.interceptor.EmailIntent;
import com.lead.agent.interceptor.PostWelcomeIntent;
import com.lead.agent.interceptor.WelcomeIntent;
import com.lead.agent.response.LeadAgentResponse;
import com.lead.agent.response.WebhookResponse;
import com.lead.agent.service.SVGLeadCall;

@RestController
@RequestMapping("/leadAgent")
public class LeadAgentController 
{
	
	
	@Autowired
	private LeadAgentResponse leadAgentResponse;
	@Autowired
	private DataFromJson dataFromJson;
	@Autowired
	private PostWelcomeIntent postWelcomeIntent;
	@Autowired
	private WelcomeIntent welcomeIntent;
	@Autowired
	private CustomerGenderIntent customerGenderIntent;
	@Autowired
	private CustomerEmailIntent customerEmailIntent;
	@Autowired
	private CustomerMobileNumberIntent customerMobileNumberIntent;
	@Autowired 
	private CustomerDOBIntent customerDobIntent;
	@Autowired
	private EmailIntent emailIntent;
	@Autowired
	private CustomerSmokerIntent customerSmokerIntent;
	@Autowired
	private Button button;
	@Autowired
	private SVGLeadCall svgLeadCall;
	@Autowired
	private Adoptionlogs adoption;

	static Map<String,Map<String,String>> ExternalMap = new ConcurrentHashMap<String,Map<String,String>>();

	@RequestMapping(method = RequestMethod.POST)
	public WebhookResponse webhook(@RequestBody String obj) {
		ResourceBundle res = ResourceBundle.getBundle("application");
		Map<String,String> internalMap = new ConcurrentHashMap<String,String>();
		System.out.println("Testing :-"+obj);
		InnerData innerData = new InnerData();
		System.out.println("Inside Controller");
		System.out.println(obj.toString());
		String speech="", sessionId = "", action="", customerName="";
		String gender="", custEmail="", mobileNum="", date="", smoke="",resolvedQuery="";
		String source="", channel="",  company="", category="";
		Date login =new Date();
		String pattern = "MM/dd/yyyy HH:mm:ss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String login_time = simpleDateFormat.format(login);
	
		
		
		try {
			System.out.println("start..");
			JSONObject object = new JSONObject(obj.toString());
			System.out.println("Request	 "+obj.toString());
			sessionId = object.get("sessionId")+"";
			
			String SessionIdLogin = sessionId+"login_time";
			internalMap.put(SessionIdLogin, login_time);
			System.out.println("date:: :: "+login_time);
			
			action = object.getJSONObject("result").get("action") + "";
			System.out.println("Action----"+action);
			resolvedQuery = object.getJSONObject("result").get("resolvedQuery")+"";
			if("welcome".equalsIgnoreCase(action))
			{
				try {
					internalMap.put("start", "start");
					ExternalMap.put(sessionId, internalMap);
					String [] splitResolvedQuery = resolvedQuery.split("#");
					source=splitResolvedQuery[1];
					channel=splitResolvedQuery[2];
					company=splitResolvedQuery[3];
					if("WebsiteIndirect".equalsIgnoreCase(company)){
						company = "Website Indirect";
					}
					else if("WebsiteDirect".equalsIgnoreCase(company)){
						company="Website Direct";
					}
					else if("DirectNatural".equalsIgnoreCase(company)){
						company="Direct Natural";
					}
					else if("WebsiteDirect-mobile".equalsIgnoreCase(company)){
						company="Website Direct-mobile";
					}
					category=splitResolvedQuery[4];
					if("WebsiteIndirect".equalsIgnoreCase(category)){
						category = "Website Indirect";
					}
					else if("WebsiteDirect".equalsIgnoreCase(category)){
						category="Website Direct";
					}
					else if("DirectNatural".equalsIgnoreCase(category)){
						category="Direct Natural";
					}
					else if("WebsiteDirect-mobile".equalsIgnoreCase(category)){
						category="Website Direct-mobile";
					}
					ExternalMap.get(sessionId).put("source", source);
					ExternalMap.get(sessionId).put("channel", channel);
					ExternalMap.get(sessionId).put("company", company);
					ExternalMap.get(sessionId).put("category", category);
				}catch(Exception ex)
				{
					speech=res.getString("welcomeCatch");
					action="default";
				}
			}
			if(!"welcome".equalsIgnoreCase(action) && !"default".equalsIgnoreCase(action))
			{
				customerName=dataFromJson.customerNameVariable(object,sessionId, ExternalMap, internalMap);
				gender=dataFromJson.genderVariable(object,sessionId, ExternalMap, internalMap);
				custEmail=dataFromJson.email_Variable(object, sessionId, ExternalMap, internalMap);
				mobileNum=dataFromJson.mobile_Variable(object, sessionId, ExternalMap, internalMap);
				date=dataFromJson.date_Variable(object, sessionId, ExternalMap, internalMap);
				smoke=dataFromJson.smoke_Variable(object, sessionId, ExternalMap, internalMap);
			}
			switch(action.toUpperCase())
			{
			//Welcome 1
			case "WELCOME":
			{
				speech=res.getString("welcome");
				innerData = button.getButtonsGender();
			}
			break;
			
			//Gender -2
			case "INPUT.GENDER":
			{
				speech=customerGenderIntent.customerGenderIntent(ExternalMap, sessionId);
				innerData = button.getButtonsYesNo();
			}
			break;
			//Smoker -3 
			case "INPUT.SMOKER":
			{
				speech=customerSmokerIntent.customerSmokerIntent(ExternalMap, sessionId);
				innerData = null;
			}
			break;
			//DOB - 4
			case "INPUT.DOB":
			{
				speech=customerDobIntent.customerDOBIntent(ExternalMap, sessionId);
			}
			break;
			
			// DOB -5
			case "INPUT.NAME":
			{
				speech=emailIntent.emailIntent(ExternalMap, sessionId);
			}
			break;
			//mobile -6
			case "INPUT.EMAIL":
			{
				speech=customerMobileNumberIntent.customerMobileNumberIntent(ExternalMap, sessionId);
				innerData = null;
			}
			break;
			// Email -7
			case "INPUT.MOBILE":
			{
				speech=customerEmailIntent.customerEmailIntent(ExternalMap, sessionId);
				innerData = null;
				//System.out.println("Channel is for SVG ---"+ExternalMap.get(sessionId).get("channel")+"");
				//String getChannel = ExternalMap.get(sessionId).get("channel")+"";
				/*****************Alsways keep this service commented for UAT and dev**************/
				/*if("34".equalsIgnoreCase(getChannel))
				{
					final String session=sessionId;
					try {
						Thread t1=new Thread(new Runnable() 
						{
							public void run() 
							{
								svgLeadCall.svgLeadCall(ExternalMap, session);
								System.out.println("----Lead Created on SVG.....");
							}
						});
						t1.start();
						t1.join();
					}
					catch(Exception ex)
					{
						System.out.println();
					}
				}*/
				
				/*********************Reason :: It will save the lead on prod environment****************/
			}
			break;
			//For clear the cache
			case "CLOSE":
			{
				ExternalMap.clear();
				internalMap.clear();
				speech=res.getString("close");

			}
			break;
			default : 
			{
				System.out.println("Intent Not match with skill,Please connect to application owner");
				speech=res.getString("default");
			}
			}
		}catch(Exception ex)
		{
			System.out.println("Exception Occoured");
			speech="Communication glitch while calling API's.";
		}
		
		
		String dbSessionId = sessionId, dbActionPerformed=action;
		String dbPolicyNumber="",  dbResolvedQuery = resolvedQuery;
		String dbspeech;
		String value =dbSessionId+"login_time";
		String dbSSOId = internalMap.get(value);
		
		if("INPUT.EMAIL".equalsIgnoreCase(action))
				{
			String speech1[]=speech.split("<hr>");
			dbspeech=speech1[0];
				}
		else{
			dbspeech=speech;
		}
		
		try{
			//logger.info("Adoption Logs :: START");
			Thread t1=new Thread(new Runnable() 
			{
				public void run() 
				{
					System.out.println("Run Method Start");
					String status=adoption.adoptionlogsCall(dbSessionId, dbSSOId, dbActionPerformed, dbResolvedQuery, dbspeech);
					System.out.println("Adoption log status :: "+status);
				}
			});
			t1.start();
			t1.join();
			//logger.info("Adoption Logs :: END");
		}catch(Exception ex)
		{
			System.out.println("Excption Occoured while saving data in to the database");
		}
		
		System.out.println("Final Speech--"+speech);
		WebhookResponse responseObj = new WebhookResponse(speech, speech, innerData);
		return responseObj;
	}

}
